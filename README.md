# exam-小猫在线考试
## 在线演示
<a href="http://47.92.221.134:8080" target="_blank">去演示</a>

## 功能介绍
###### 题库：支持单选、多选、填空、判断、问答
###### 试卷：支持人工组卷、随机组卷、防作弊
###### 考试：组织考试、阅卷、统计、邮件通知
###### 练习：试题模拟、试题评论
###### 系统：发布公告、自动备份、系统错误发邮件
###### 自定义：企业名称、LOGO、备份目录、密码策略

## 角色划分
###### 管理员：基础信息维护、考试维护、主观题阅卷，数据统计等
###### 用户：模拟练习、参加考试、查阅成绩等

## 应用场景
###### 应用场景：企业内部组织考试、面试者扫码答题
###### 简单易用：支持win界面启动，开箱即用；分步导航设计，可快速组织一场考试
###### 功能限制：无任何限制，支持1000人同时在线答题（中配电脑、虚拟机、数据库等优化后）
###### 持续更新：专业团队运营，默认每月底发布新版本


## 账号密码
###### 管理员 admin 111111

## 相关文档
<a href="doc">接口文档</a>
<a href="doc">部署文档</a>
<a href="img/process">状态流转图</a>
<a href="h5">PC端更新</a>
<a href="src">后端更新</a>
<a href="m">移动端更新</a>

## 界面预览
<a href="img/webpage">去查看</a>

## 技术实现
bs架构：jdk8、mysql5.7、springboot2x、element-plus（vue3）

## 技术交流
软件bug：可直接提Issues或提qq群（811189776），按严重程度决定修复版本的时间<br/>
通用需求：可以影响版本走向，按排版进度加入到之后的某个版本<br/>
定制需求：收费服务，商业合作qq（3532748782）<br/>
开源不易：如果觉得本软件帮助了您，帮忙在gitee/github点赞<br/>
欢迎加入：鼓励贡献代码，鼓励二次开发<br/>
![输入图片说明](img/9.png)
<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=Vqhsz3XUUg-SS4m8LM0mrL3WcnKrL9xo&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="在线考试技术交流群" title="在线考试技术交流群"></a>

## 开源项目推荐
<a href="http://www.wcpknow.com" target="_blank">产品介绍</a><br/>
<a href="https://gitee.com/macplus/WCP" target="_blank">WCP:知识管理系统</a>（内部资料上传检索知识共享）<br/>
<a href="https://gitee.com/macplus/WDA" target="_blank">WDA:文件转换组件</a>（附件在线预览）<br/>
<a href="https://gitee.com/macplus/WTS" target="_blank">WTS:在线答题系统</a><br/>
<a href="https://gitee.com/macplus/WLP" target="_blank">WLP:在线学习系统</a><br/>
<a href="https://gitee.com/macplus/plogs" target="_blank">PLOGS:项目任务日志管理系统</a>（项目进度跟踪）<br/>